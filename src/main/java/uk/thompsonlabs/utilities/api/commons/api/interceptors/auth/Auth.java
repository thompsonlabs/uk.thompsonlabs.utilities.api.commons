package uk.thompsonlabs.utilities.api.commons.api.interceptors.auth;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Auth {

    int requiredUserLevel();

    String callerIdHeaderName() default "cId";

    String callerUserLevelHeaderName() default "cUl";

    String callerSessionIdHeaderName() default "X-AUTH-KEY";


}
