package uk.thompsonlabs.utilities.api.commons.api.response;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import uk.thompsonlabs.utilities.api.commons.api.request.search.Search;
import uk.thompsonlabs.utilities.api.commons.api.request.search.SearchManager;


import java.util.Collection;

/**
 *  A collection of utility method to aid in the preparation and
 *  generation of a service response.
 * */
public class ServiceResponseHelper {

    private static final SearchManager searchManager = SearchManager.newInst();

    public static String API_VERSION;

    public static String ENVIRONMENT;

    /**
     * Where the listing option array is not null and contains an entry then that entry is returned.
     *  NOTE: there will only ever be a SINGLE entry we use an array as the ListingOption is passed in to
     *       services as a var-arg parameter.
     * @param  listingOptions An array of ListingOptions obtained from var-args should contain a single item at most.
     * @return ListingOptions A listing options instance, or null where the provided ListingOptions array is null.
     **/
    public static ListingOptions parseListingOptions(final ListingOptions[] listingOptions){

       return (listingOptions == null || listingOptions.length < 1) ? null : listingOptions[0];

    }

    /** Builds JPA Pageable instance from the supplied listingOptions instance.
     * @param listingOptions A ListingOption instance.
     * @return Pageable A pageable instance according to the provided ListingOptions. */
    public static Pageable buildPageableFromListingOptions(ListingOptions listingOptions){

        Pageable pageableFromListingOptions;

        //Whilt the user will specify a page index from 1 the repository we utilised in zero indexed
        //thus we subtract 1 from the value specified by the user. We FORCE the value to be zero where
        //a value of zero or less is specified...
        int zeroIndexedPageNumber = Math.max((listingOptions.getPageNumber() - 1), 0);

        if(listingOptions.getSort() == null) {


            pageableFromListingOptions = PageRequest.of(zeroIndexedPageNumber,
                                                        listingOptions.getResultsPerPage());

        }
        else{

            pageableFromListingOptions = PageRequest.of(zeroIndexedPageNumber,
                                                        listingOptions.getResultsPerPage(),
                                                        Sort.by(Sort.Direction.valueOf(listingOptions.getSort().getSortOrder().name()),
                                                                                       listingOptions.getSort().getProperties()));
        }

        return pageableFromListingOptions;

    }

    /** Builds a JPA Specification containing one or more search Criteria from the Search instance
     *  referenced in the provided listingOptions object. Specifications encapsulate all the search
     *  criteria that should be applied during a lookup operation undertaken on the underlying data store.
     *  @param listingOptions A ListingOptions instance.
     *  @return Specification A specification containing all the search criteria that should be applied
     *                        to the look up operation on the underlying data store or NULL where the
     *                        referenced Search instance is null or contains less than one SearchCriteria.
     **/
    public static Specification buildSpecificationFromListingOptionsSearch(Search search){

        //isssue a call to our SearchCompiler to actually undertake the work of generating
        //the Specification and return the result.
        return searchManager.compileSearch(search);
    }


    /** Builds a Service.Pagination instance from the supplied ListingOptions and Page instances. */
    public static <T> ServiceResponse.Pagination buildPagination(ListingOptions listingOptions, Page<T> page){

        final var resultsPerPage = listingOptions.getResultsPerPage();

        final var currentPage = listingOptions.getPageNumber();

        final var totalPages = page.getTotalPages();

        final var totalCount = page.getTotalElements();

        final var pagination = ServiceResponse.Pagination.newInst(resultsPerPage,currentPage,totalPages,totalCount);

        return pagination;
    }

    /**
     * Creates a new ServiceResponse pre-populated with API_VERSION an ENVIRONMENT variables
     * */
    public static <T> ServiceResponse<T> newServiceResponse(T resultData){

        if(API_VERSION == null)
            throw new NullPointerException("API Version has not been set.");

        if(ENVIRONMENT == null)
            throw new NullPointerException("Envrionment has not been set.");

        ServiceResponse<T> serviceResponse = new ServiceResponse<>(API_VERSION,
                                                                   ENVIRONMENT,
                                                                   resultData);


        return serviceResponse;
    }

    /**
     * Creates a new ServiceResponse pre-populated with API_VERSION an ENVIRONMENT variables
     * */
    public static <T> ServiceResponse<T> newServiceResponse(Collection<T> collectionResultData, ServiceResponse.Pagination pagination){

        if(API_VERSION == null)
            throw new NullPointerException("API Version has not been set.");

        if(ENVIRONMENT == null)
            throw new NullPointerException("Envrionment has not been set.");

        //create service response.
        ServiceResponse<T> serviceResponse = new ServiceResponse<>(API_VERSION,
                                                                   ENVIRONMENT,
                                                                   collectionResultData,
                                                                   pagination);


        return serviceResponse;
    }
}
