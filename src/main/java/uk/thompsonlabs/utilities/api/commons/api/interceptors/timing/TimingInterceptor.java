package uk.thompsonlabs.utilities.api.commons.api.interceptors.timing;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import uk.thompsonlabs.utilities.api.commons.api.context.APIRequestContext;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Used to intercept applicable request (i.e those annotated with the @Time anotation) and signal the
 * APIRequestContext to log the request start time. This component is also repsonsible for clear all context data
 * (where it exists) post return of a response to the client.
 *
 * Note: This component ONLY logs the start time, the @see TimingRestControllerAdvice is responsible for logging
 *       the method execution end time and for injecting that value into the outgoing response to the client.
 * */
@Component
public class TimingInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HandlerMethod hm;

        try {

            hm = (HandlerMethod) handler;
        }
        catch (ClassCastException e) {

            return HandlerInterceptor.super.preHandle(request, response, handler);
        }

        Method method = hm.getMethod();

        var methodName = method.getName();

        //exclude internal Spring Framework methods
        if(methodName.equals("uiConfiguration")
                || methodName.equals("securityConfiguration")
                || methodName.equals("swaggerResources")
                || methodName.equals("getDocumentation") )
            return  HandlerInterceptor.super.preHandle(request, response, handler);

        final Time timeAnnotation = method.getAnnotation(Time.class);

        if(timeAnnotation == null) {

            return HandlerInterceptor.super.preHandle(request, response, handler);
        }
        else{

            Logger.getLogger(TimingInterceptor.class.getName()).log(Level.INFO,"Time annotation found, logging Method: "+methodName+" execution timings..");

            APIRequestContext.getInstance().logStartTime();
        }

        return true;
    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {

        //Clean up...

        //Clear APIRequestContext data
        APIRequestContext.getInstance().clearAllData();

        Logger.getLogger(TimingInterceptor.class.getName()).log(Level.INFO,"Cleaned up APIRequestContext data post response to client.");
    }

}
