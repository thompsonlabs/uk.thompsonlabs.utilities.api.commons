package uk.thompsonlabs.utilities.api.commons.api.request.search;

import org.springframework.data.jpa.domain.Specification;

public interface SearchManager {

    static SearchManager newInst(){

        return new DefaultSearchManager();
    }

    Search parseSearch(String queryString);

    Specification compileSearch(Search search);
}
