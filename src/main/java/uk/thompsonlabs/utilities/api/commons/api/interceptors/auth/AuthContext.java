package uk.thompsonlabs.utilities.api.commons.api.interceptors.auth;



public final class AuthContext {

    private final String callerId;

    private final String callerUserLevelStr;

    private final String callerSessionId;

    private final int requiredUserLevel;

    private final int callerUserLevel;

    private AuthContext(final String callerId,
                        final String callerUserLevelStr,
                        final String callerSessionId,
                        final int requiredUserLevel) throws AuthenticationException, AuthorizationException {


        if(callerId == null)
            throw new AuthenticationException("Unauthorised. A null or invalid Caller ID was specified.");

        if(callerUserLevelStr == null)
            throw new AuthenticationException("Unauthorised. A null or invalid Caller User Level was specified.");

        if(callerSessionId == null)
            throw new AuthenticationException("Unauthorised. A null or invalid Caller Session ID was specified.");

        this.requiredUserLevel = requiredUserLevel;

        try {

            this.callerUserLevel = Integer.parseInt(callerUserLevelStr);
        }
        catch(NumberFormatException nfe){

            throw new AuthenticationException("Unauthorised. A null or invalid Caller User Level was specified: "+nfe.getMessage(),nfe);
        }

        checkAuthorization(this.callerUserLevel,this.requiredUserLevel);

        this.callerId = callerId;
        this.callerUserLevelStr = callerUserLevelStr;
        this.callerSessionId = callerSessionId;

    }

    /**
     *  Creates new AuthContext instance.
     *  NB: This factory method is ONLY intended for use by the AuthInterceptor and SHOULD NOT be
     *      accessed directly.
     * */
    static AuthContext newInst(final String callerId,
                               final String callerUserLevel,
                               final String callerSessionId,
                               final int requiredUserLevel) throws AuthenticationException, AuthorizationException {

        return new AuthContext(callerId,callerUserLevel,callerSessionId,requiredUserLevel);
    }

    /** Alternative constructor, creates AuthContext from a Map of header name-value pairs.
    public static AuthContext fromHeaders(final Map<String,String> headersNameValuePairs,
                               final int requiredUserLevel,
                               final String callerIdHeaderName,
                               final String callerUserLevelHeaderName,
                               final String callerSessionIdHeaderName) throws AuthenticationException, AuthorizationException {


        var callerId = headersNameValuePairs.get(callerIdHeaderName);
        var callerUserLevel = headersNameValuePairs.get(callerUserLevelHeaderName);
        var callerSessionId = headersNameValuePairs.get(callerSessionIdHeaderName);

        return new AuthContext(callerId,callerUserLevel,callerSessionId,requiredUserLevel);
    }

     */
    public String getCallerId() {
        return callerId;
    }

    public int getRequiredUserLevel() {
        return requiredUserLevel;
    }

    public int getCallerUserLevel() {
        return callerUserLevel;
    }

    public String getCallerSessionId() {
        return callerSessionId;
    }

    public static void checkAuthorization(int specifiedUserLevel,int requiredUserLevel) throws AuthorizationException {

        if(specifiedUserLevel < requiredUserLevel)
            throw new AuthorizationException("Forbidden. The caller does not have sufficient privileges to view the specified resource.");
    }


                                                //HELPER CLASSES

    public static class AuthenticationException extends Exception{

        public AuthenticationException() {
        }

        public AuthenticationException(String message) {
            super(message);
        }

        public AuthenticationException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static class AuthorizationException extends Exception{

        public AuthorizationException() {
        }

        public AuthorizationException(String message) {
            super(message);
        }

        public AuthorizationException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
