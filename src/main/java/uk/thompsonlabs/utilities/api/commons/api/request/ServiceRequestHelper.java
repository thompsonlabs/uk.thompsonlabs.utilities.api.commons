package uk.thompsonlabs.utilities.api.commons.api.request;

import uk.thompsonlabs.utilities.api.commons.api.request.search.Search;
import uk.thompsonlabs.utilities.api.commons.api.request.search.SearchManager;
import uk.thompsonlabs.utilities.api.commons.api.request.search.SearchOp;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParsePosition;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** A collection of utility methods to aid in working with inbound service requests. */
public final class ServiceRequestHelper {

    private static final SearchManager searchManager = SearchManager.newInst();

    public static Search parseSearchFromQueryString(String searchQueryString){

        return searchManager.parseSearch(searchQueryString);

    }



    public static void main(String[] args){

        //System.out.println("Supplied search was: ");

        //var parsedSearch = parseSearchFromQueryString("lastName:doe,age>25,signupDate<2,'lastName!thompson,email~hotmail*");

        //var parsedSearch = parseSearchFromQueryString("id:147b4788,'surname:thompson");

        var parsedSearch = parseSearchFromQueryString("id:147b47884545,surname:thompson");

        parsedSearch.getAllSearchCriteria().stream().map(e->{return "key: "+e.getKey()+" operation: "+e.getSearchOperation().name()+" value: "+e.getValue()+" isOrCriteria: "+e.isOrCriteria();}).forEach(System.out::println);

        /**
        final var val = "going";
        final var decimalFormatObj = new DecimalFormat();
        decimalFormatObj.setParseBigDecimal(true);
        final BigDecimal parsedVal = (BigDecimal) decimalFormatObj.parse(val,new ParsePosition(0));
        */


    }

    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

}
