package uk.thompsonlabs.utilities.api.commons.api.response;


import uk.thompsonlabs.utilities.api.commons.api.request.search.Search;

public final class ListingOptions {

    /**The page number to fetch, defaults to one where no value is specified. */
    private final int pageNumber;

    /** The number of results to fetch per page. Defaults to 10 where no value is specified. */
    private final int resultsPerPage;

    /** The parameters to sort by, defaults to NULL where no value is specified. */
    private final Sort sort;

    /** The criteria(s) to search on defaults to null where no value is specified. */
    private final Search search;

    private final boolean includedDeleted;

    private final boolean includeInactive;

    public ListingOptions() {

        this.pageNumber  = 1;

        this.resultsPerPage = 10;

        this.sort= null;

        this.search = null;

        this.includedDeleted = false;

        this.includeInactive = false;
    }

    public ListingOptions(int pageNumber,
                          int resultsPerPage,
                          Sort sort,
                          Search search,
                          boolean includedDeleted,
                          boolean includeInactive) {


        this.pageNumber = pageNumber;
        this.resultsPerPage = resultsPerPage;
        this.sort = (sort== null || sort.getProperties() == null || sort.getProperties().length < 1) ? null : sort;
        this.search = search;
        this.includedDeleted = includedDeleted;
        this.includeInactive = includeInactive;
    }

    public static ListingOptions defaults(){

        return new ListingOptions();
    }

    public static ListingOptions newInst(int pageNumber,
                                         int resultsPerPage,
                                         Sort sort,
                                         Search search,
                                         boolean includedDeleted,
                                         boolean includeInactive){

        return new ListingOptions(pageNumber,
                                  resultsPerPage,
                                  sort,
                                  search,
                                  includedDeleted,
                                  includeInactive);
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public int getResultsPerPage() {
        return resultsPerPage;
    }

    public Sort getSort() {
        return sort;
    }

    public Search getSearch(){

        return this.search;
    }

    public boolean isIncludedDeleted() {
        return includedDeleted;
    }

    public boolean isIncludeInactive() {
        return includeInactive;
    }


    public static class Sort{

        private final SortOrder sortOrder;

        private final String[] properties;

        Sort(SortOrder sortOrder, String[] properties) {
            this.sortOrder = sortOrder;
            this.properties = properties;
        }

        public static Sort newInst(SortOrder sortOrder, String... properties){

            return new Sort(sortOrder,properties);
        }

        public SortOrder getSortOrder() {
            return sortOrder;
        }

        //Note we return the properties as a List of strings since
        //thats how it will be injested by the JPA repository Sort object.
        public String[] getProperties() {

            return properties;

        }
    }

    public static enum SortOrder{
        ASC,
        DESC
    }
}
