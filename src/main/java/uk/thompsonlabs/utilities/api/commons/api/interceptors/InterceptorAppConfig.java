package uk.thompsonlabs.utilities.api.commons.api.interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import uk.thompsonlabs.utilities.api.commons.api.interceptors.auth.AuthInterceptor;
import uk.thompsonlabs.utilities.api.commons.api.interceptors.timing.TimingInterceptor;

@Component
public class InterceptorAppConfig implements WebMvcConfigurer {

    @Autowired
    AuthInterceptor authInterceptor;

    @Autowired
    TimingInterceptor timingInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(authInterceptor);

        registry.addInterceptor(timingInterceptor);
    }
}
