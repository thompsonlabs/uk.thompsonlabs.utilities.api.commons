package uk.thompsonlabs.utilities.api.commons.api.interceptors.auth;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import uk.thompsonlabs.utilities.api.commons.api.context.APIRequestContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class AuthInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {


        HandlerMethod hm;

        try {

            hm = (HandlerMethod) handler;
        }
        catch (ClassCastException e) {

            return HandlerInterceptor.super.preHandle(request, response, handler);
        }

        Method method = hm.getMethod();

        var methodName = method.getName();

        //exclude internal Spring Framework methods
        if(methodName.equals("uiConfiguration")
                || methodName.equals("securityConfiguration")
                || methodName.equals("swaggerResources")
                || methodName.equals("getDocumentation") )
            return  HandlerInterceptor.super.preHandle(request, response, handler);



        final Auth authAnnotation = method.getAnnotation(Auth.class);

        if(authAnnotation == null) {

            return HandlerInterceptor.super.preHandle(request, response, handler);
        }
        else{

            Logger.getLogger(AuthInterceptor.class.getName()).log(Level.INFO,"Auth annotation found, preparing AuthContext.. ");

            var requiredUserLevel = authAnnotation.requiredUserLevel();

            var callerIdHeaderName = authAnnotation.callerIdHeaderName();

            var callUserLevelHeaderName = authAnnotation.callerUserLevelHeaderName();

            var callerSessionIdHeaderName = authAnnotation.callerSessionIdHeaderName();


            //construct AuthContext
            final AuthContext authContext = AuthContext.newInst(request.getHeader(callerIdHeaderName),
                                                                request.getHeader(callUserLevelHeaderName),
                                                                request.getHeader(callerSessionIdHeaderName),
                                                                requiredUserLevel);


            //Append AuthContext to APIRequestContext
            APIRequestContext.getInstance().setAuthContext(authContext);


            //Add all Headers to the APIRequestContext
            Map<String,String> allHeadersMap = new HashMap<>();
            request.getHeaderNames().asIterator().forEachRemaining(e->{ allHeadersMap.put(e,request.getHeader(e));});
            APIRequestContext.getInstance().setHeaders(allHeadersMap);

            Logger.getLogger(AuthInterceptor.class.getName()).log(Level.INFO,"Successfully prepared AuthContext.");

            return true;
        }
    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {

        //Clean up...

        //Clear APIRequestContext data
        APIRequestContext.getInstance().clearAllData();

        Logger.getLogger(AuthInterceptor.class.getName()).log(Level.INFO,"Cleaned up APIRequestContext data post response to client.");
    }
}
